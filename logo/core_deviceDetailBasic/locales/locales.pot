msgid ""
msgstr ""
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Project-Id-Version: \n"

#: core_deviceDetailBasic/index.js:98
msgid "Info"
msgstr ""

#: core_deviceDetailBasic/index.js:60
msgid "Inventory update operation created!"
msgstr ""

#: core_deviceDetailBasic/index.js:63
msgid "Inventory update operation executed successfully!"
msgstr ""

#: core_deviceDetailBasic/index.js:67
msgid "Inventory update operation failed with reason: {{failureReason}}!"
msgstr ""

#: core_deviceDetailBasic/index.js:69
msgid "Inventory update operation failed!"
msgstr ""

#: core_deviceDetailBasic/index.js:40
#: core_deviceDetailBasic/index.js:48
msgid "Request inventory update"
msgstr ""
