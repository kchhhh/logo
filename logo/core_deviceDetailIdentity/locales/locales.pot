msgid ""
msgstr ""
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Project-Id-Version: \n"

#: core_deviceDetailIdentity/views/identity.html:6
msgid "Add identity"
msgstr ""

#: core_deviceDetailIdentity/index.js:40
msgid "Can't find managed for identity {{type}}: {{externalId}}!"
msgstr ""

#: core_deviceDetailIdentity/views/identity.html:28
msgid "Cancel"
msgstr ""

#: core_deviceDetailIdentity/controllers/identity.js:28
msgid "Confirm delete?"
msgstr ""

#: core_deviceDetailIdentity/views/identity.html:46
msgid "Delete identity"
msgstr ""

#: core_deviceDetailIdentity/index.js:37
msgid "Device found for identity {{type}}: {{externalId}}"
msgstr ""

#: core_deviceDetailIdentity/controllers/identity.js:29
msgid "Do you really want to remove the external identity \"{{externalId}}\" of the type {{idType}} ?"
msgstr ""

#: core_deviceDetailIdentity/controllers/identity.js:41
msgid "External ID created"
msgstr ""

#: core_deviceDetailIdentity/controllers/identity.js:35
msgid "External ID deleted"
msgstr ""

#: core_deviceDetailIdentity/views/identity.html:22
#: core_deviceDetailIdentity/views/identity.html:37
msgid "Id"
msgstr ""

#: core_deviceDetailIdentity/index.js:12
msgid "Identity"
msgstr ""

#: core_deviceDetailIdentity/views/identity.html:32
msgid "No external ids found!"
msgstr ""

#: core_deviceDetailIdentity/views/identity.html:27
msgid "Save"
msgstr ""

#: core_deviceDetailIdentity/controllers/identity.js:7
msgid "The external Id is something that links the device information and the physical world (ex.: Serial Number, Imei). If for some reason the hardware needs to be replaced, so must the external id be replaced with the appropriate information. With this procedure the new hardware can continue linked to the information that was created by its predecessor"
msgstr ""

#: core_deviceDetailIdentity/views/identity.html:18
#: core_deviceDetailIdentity/views/identity.html:36
msgid "Type"
msgstr ""
