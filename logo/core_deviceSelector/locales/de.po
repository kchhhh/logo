msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: \n"
"PO-Revision-Date: \n"
"Last-Translator: \n"
"Language: de\n"
"Language-Team: \n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"MIME-Version: 1.0\n"
"X-Generator: Poedit 1.8.9\n"

msgid "Deselect all"
msgstr "Auswahl aufheben"

msgid "Device search"
msgstr "Gerätesuche"

msgid "Device {{id}}"
msgstr "Gerät {{id}}"

msgid "Device's name or value of any device's property"
msgstr "Name oder anderer Attributwert des Geräts"

msgid "No assets or devices found for '{{searchData.lastText}}'"
msgstr "Keine Assets oder Geräte mit dem Suchbegriff '{{searchData.lastText}}' gefunden."

msgid "No results found."
msgstr "Keine Ergebnisse gefunden."

msgid "No search done yet. Use the textbox above to search for devices."
msgstr "Bitte benutzen Sie das obige Textfeld um nach Geräten zu suchen."

msgid "Root Device"
msgstr "Hauptgerät"

msgid "Search"
msgstr "Suche"

msgid "Searching devices"
msgstr "Suche Geräte"

msgid "Select all"
msgstr "Alle auswählen"
